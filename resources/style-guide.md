---
title: Style Guide
description: Markdown style guide for writing tidbit and article posts
published: true
date: 2021-06-06T06:17:14.884Z
tags: resource, style, markdown
editor: markdown
dateCreated: 2021-06-06T06:16:43.613Z
---

This page outlines what markdown styles mean when used in posts on this site. It also defines how certain often repeated concepts should be used.