---
title: Tidbit Template
description: Sample outline/structure of a tidbit post
published: true
date: 2021-06-06T06:17:53.899Z
tags: tidbit, template
editor: markdown
dateCreated: 2021-06-06T06:12:41.730Z
---

Introductionary sentence/paragraph of what the command is and why it may be useful. Please refer to the [style guide](resources/style-guide) while writing your post.

## Use H2 or Greater
If there are a few related commands but for clarity should be seperated, e.g. different commands due to different operating systems, use an H2 or H3 to seperate the sections.

When displaying a single line command use `inline` code markdown.

When displaying multi-line command, or command output, use code block markdown.
```
Such as this
```

**References**
* List any sources that was used in the creation of this tidbit