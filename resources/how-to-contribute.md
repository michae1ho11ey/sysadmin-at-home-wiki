---
title: How to Contribute
description: How to contribute tidbits and articles to SysAdminAtHome.com
published: true
date: 2021-06-06T04:22:53.328Z
tags: contribution, resource
editor: markdown
dateCreated: 2021-06-06T04:22:34.902Z
---

Though the bulk of the tidbits and articles on this site will be written by Michael, he would love to receive help in adding more information to help all those who want to run servers in their own home. Below you'll find resources and explanations of how contribute tidbits and articles to the site.

### Resources
The following links point to templates for creating a new tidbit or article and a style guide for all contributions. Please look over the style guide and follow it for whatever contribution you provide.

- [Tidbit Template](/resources/templates/article.md)
- [Article Template](/resources/templates/tidbit.md)
- [Style Guide](/resources/sytle-guide)

## Contribute via GitLab
Wiki.js is a database driven wiki system that officially allows edits via their web ui, but they allow for a "backup" to a git repo which is saved as markdown files. This "backup" allows for bidirectional changes meaning if a change is made at the repo those changes will be synced back up into the wiki after about five minutes. This feature give options when contributing in different ways.

### Advantages
* Posts (tidbit or article) can be written using a text editor of choice.
* Contributions don't require a registered account on [sysadminathome.com](https://sysadminathome.com).
* Access to a spell checker in your text editor.

### Disadvantages
* Process from page creation to publishing is several more steps than using the main UI.
* There is a five minute delay from the time the code is pushed to the master branch and synced in the wiki.
* Can only add new documents or update existing ones. Moving or deleting documents can only be done via the main UI.

1. [Fork the repo](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
2. Pull down your forked repo locally
3. Add a new tidbit or article by copying a template from `resources/templates/tidbit.md` or `resources/templates/article.md`
4. Name the copied file to be a hyphenated version of the title with the 'md' extension
5. Edit the file and update the data listed between the '---' (this is called the front matter)
6. Add your content as defined in the template and the [style guide](/resources/sytle-guide)
7. Update the appropriate base page (the 'articles.md' or 'tidbits.md' files at the root root of the repo) with a link to your post
  - Please list your post on the base page in alphabetic order
  - Use a relative path path assuming the base of the repo being the root
8. Push your changes up to your forked repo
9. Create a merge request (MR) back to the main repo on the master branch

## Contribute via Wiki.js
The main way to add or update content is by a registered user in the main UI for the wiki. The way this wiki is set up the only way to add entries is by a logged in and registered users.

### Advantages
* The process from page creation to publishing is just a couple of steps.
* There is no delay between publishing and being visible online.
* Less manual steps required.

### Disadvantage
* Requires an account to post, and not everyone can have an account.
* Owner/moderator isn't notified when a new post is created or edited.
* The markdown editor doesn't currently support spell checking.

1. Log into the website and click on the 'New Page" icon (sheet icon with plus symbol) in the upper right hand corner
2. Click or type the desired path--either `/articles/` or `/tidbits/` and a hyphenated title as the name
3. Click on the Templates button from the editor chooser and then click on the folder 'resources' then 'templates' and select the post style
4. Update the title, description (if needed, if not leave it blank), and then add relevant tags
  - Every post must have a minimum of one tag, the post type, e.g. 'tidbit' or 'article
  - When adding tags that would be multi-word, use hyphens instead of a space
5. Once the page loads, replace the template instructions with the information you'd like to contribute then click on the 'Create' button when done
6. Navigate back to the main [Tidbits](/tidbits) or [Articles](/articles) page and edit that page and add the post (in alphabetic order)