---
title: Home
description: 
published: true
date: 2021-06-06T05:59:55.916Z
tags: 
editor: markdown
dateCreated: 2021-05-12T14:45:53.740Z
---

Welcome to **SysAdmin @ Home**. The goal of this site is simple and reminiscent of the early goal of Microsoft to put a copmuter on every desk. **This site's goal is to provide the knowledge needed for anyone to acquire and maintain a home server!** 

This wiki has a few sections: Tidbits, Articles, and Sys Admin 101.

Tidbits are little one-off commands picked up along the way to help get things done, like copying a folder from one place to another using rsync. They provide minimal documentation and mainly uased as reference.

The Articles section holds more detailed explanations of how to setup various applications and general knowledge.

Sys Admin 101 is the a course-like articles/podcasts on how to be a Linux systems administrator. Over time the goal of this section is to cover the kind of content you'd expect to learn in a college course on the subject.

- [Tidbits *Small and useful one-off commands*](tidbits)
- [Articles *Susinct notes on larger/multi-step projects*](articles)
- [Sys Admin 101 *Course-like articles on being a sys admin*](sys-admin-101)
{.links-list}

If you have some Linux sys admin experience and would like to share, check out our [how to contriubte](resources/how-to-contribute) page or send an email to *michael [at] sysadminathome [dot] com*.