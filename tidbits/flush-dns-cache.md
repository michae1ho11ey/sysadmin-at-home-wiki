---
title: How to Flush DNS Cache
description: Commands to flush DNS cache
published: true
date: 2021-06-04T18:17:29.362Z
tags: cli, linux, tidbit, cache, dns
editor: markdown
dateCreated: 2021-06-04T18:13:06.255Z
---

## Linux
There are two ways to flush DNS; the old way or the systemd way. To determine which method you can employ use the following command to see is systemd is managing your DNS cache.

`sudo systemctl is-active systemd-resolved`

If the answer is 'active' then use the systemd steps, otherwise use the nscd steps.

### systemd
1. View cache size/stats: `sudo systemd-resolve --statistics`
2. Flush cache: `sudo systemd-resolve --flush-caches`
3. Check stats again to see if the size changed

### nscd
1. Restart the DNS service: `sudo systemctl restart nscd`



**Sources:**
* https://linuxapt.com/blog/189-how-to-flush-dns-cache-on-ubuntu-20-04
* https://vitux.com/how-to-flush-dns-cache-on-ubuntu/