---
title: Parse JSON in BASH with jq
description: 
published: true
date: 2021-06-08T02:41:48.725Z
tags: cli, tidbit, bash, json, jq
editor: markdown
dateCreated: 2021-06-08T02:41:48.725Z
---

The tool `jq` is a cli JSON parser. This tool can be used directly from the command line, but is really powerful as part of a BASH pipeline. Below are the most common filters I use with `jq`.

### Return Different Value Based on  Selection
`jq '.[] |select(.key.to.search=="Look for this string") | .return.key'`

**Example:** 
```
jq '.[] |select(.commit.sha=="749ffca9c1936811e3d0b15d2d977aac3dfd8b23") |.name'
```

### Return Results Without Quotation
`jq -r ‘.’`

The flag ‘-r’ is raw output