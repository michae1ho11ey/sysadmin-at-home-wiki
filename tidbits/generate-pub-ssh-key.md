---
title: Generate Public SSH Key
description: How to generate a public SSH key from the private key
published: true
date: 2021-05-17T14:36:53.654Z
tags: linux, macos, cli, auth, security, tidbit
editor: markdown
dateCreated: 2021-05-14T15:31:02.474Z
---

When working with AWS, it's easy to have a private SSH key, but no public key. Using the `ssh-keygen` app you can easily generate the public key from the private key.

*Note: You can't generate a private key if all you have is a public key*

### Grab fingerprint of private key
You don't actually need to run this command to generate a public key, it just provides some useful information.
```bash
ssh-keygen -lf /path/to/private-key.pem
```
The `-l` flag returns the size of the key and the fingerprint.
The `-f` flag tells ssh-keygen the following command is the path to the key file.

### Generate public key
In the following command by default would just dump the public key to the screen, but we redirect stdout to a file to save a step.

```bash
ssh-keygen -yf /path/to/private-key.pem > /path/to/pub-key.pub
```
The `-y` flag generates the public key and displayes it on screen.
The `-f` flag tells ssh-keygen the following command is the path to the key file.

**Source:** https://blog.tinned-software.net/generate-public-ssh-key-from-private-ssh-key/