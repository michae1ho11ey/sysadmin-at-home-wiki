---
title: Generate SSH Key Fingerprint
description: 
published: true
date: 2021-06-02T17:55:37.823Z
tags: cli, linux, macos, tidbit, ssh
editor: markdown
dateCreated: 2021-06-02T17:55:35.807Z
---

The following command is to come up with the fingerprint or hash of your SSH public key (this is what SSH stores in 'known_hosts' file).

Represent the fingerprint as a SHA256 hash:
```
ssh-keygen -lf keyname.pub
```

Represent the fingerprint as a MD5 hash:
```
ssh-keygen -E md5 -lf keyname.pub
```