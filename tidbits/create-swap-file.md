---
title: Create Linux Swap File
description: 
published: true
date: 2021-06-02T16:07:15.290Z
tags: cli, linux, memory, os, tidbit
editor: markdown
dateCreated: 2021-06-02T15:10:51.180Z
---

## Identify if there is any swap enabled:
`free -g`
or
`swapon --show`

## Create a Swap File
`sudo dd if=/dev/zero of=/swapfile bs=1024 count=4194304`

* To calculate file size, multiple block size (bs) by block count (count).
* The above command creates a 4 GiB swap file

## Prep Swap File for Use
* Format swap file: `sudo mkswap /swapfile`
* Set swap file permissions: `sudo chmod 600 /swapfile`

## Permenatly Enable Swap File
* Edit '/etc/fstab'
* Add the following to the bottom of the file
	* `/swapfile    none    swap    sw    0    0`

## Turn on the Swap File
* `sudo swapon /swapfile`
* Verify swap is enabled (noted above)


**Source:** https://www.howtogeek.com/455981/how-to-create-a-swap-file-on-linux/