---
title: Clearing RAM Cache and Swap
description: 
published: true
date: 2021-06-04T18:15:57.412Z
tags: linux, tidbit, ram, cache, swap, script
editor: markdown
dateCreated: 2021-06-02T19:25:02.850Z
---

## Why would anyone need this?

There are times when your server or workstation might end up using a lot of RAM cache, and then start using up swap. This is easy to rectify with a simple reboot, but if your home server needs to remain up, that wouldn't be ideal. 

Here are some simple steps to keep your server/workstation going, without a reboot, using the following commands:

### Clear swap:
```
sudo swapoff -a && sudo swapon -a
```

### Clear PageCache only:
```
sudo sysctl vm.drop_caches=1
```

### Clear dentries and inodes:
```
sudo sysctl vm.drop_caches=2
```
### Clear everything, indescriminately (my favorite): 
```
sudo sysctl vm.drop_caches=3
```

### You can also create a script, like below:
```
sudo nano /usr/bin/cleanup
```
Enter the following contents:
```
#!/bin/sh

clear
echo ""
echo "Cleaning things up..."
sleep .5
echo ""
sudo swapoff -a && sudo swapon -a
sudo sysctl vm.drop_caches=3

clear
```

Now, make it executable:
```
sudo chmod +x /usr/bin/cleanup
```

Try running it:
```
$ cleanup
```

### Wrapping up:

Verify that RAM caches and swap were cleaned up by using `htop`, `bpytop`, `bashtop`, `top`, or any other system monitoring application. You may obtain these via your package manager.

You can even append the "cleanup" script to the end of another script that tends to use a lot of RAM. 

Hopefully, this helps you maintain greater `uptime` and, if nothing else, increase your learning toward becoming a better sysadmin. 