---
title: Generate Public TLS Certificate From Private TLS Certificate
description: 
published: true
date: 2021-05-17T14:36:11.469Z
tags: encryption, security, tidbit, tls, ssl, cli, linux, macos
editor: markdown
dateCreated: 2021-05-17T14:33:54.205Z
---

There may be a situation in which you have a private SSL/TLS certificate but you need the public certificate for use with something like a webserver. Generating the public cert from the private one is trivial. It doesn't work the other way around, however.

`openssl rsa -in /path/to/cert.key -pubout -out /path/to/pub-cer.pem`