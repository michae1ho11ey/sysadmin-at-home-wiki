---
title: Checksum Verification
description: How to verify the checksum of a file via the command line
published: true
date: 2021-06-02T16:33:28.694Z
tags: linux, macos, tidbit, encryption
editor: markdown
dateCreated: 2021-05-17T14:23:20.942Z
---

A checksum is a one-way encryption method that is used to create a fingerprint or hash of a file as a way to verify that a file hasn't been modified. Generating a checksum for a file and then modifying by just one character will produce a completely different checksum.

There are two common checksum algorithms used for file verifications; `md5` and `sha`. Recently `md5` has fallen out of favor due know vulnerabilities in the algorithm. If possible, use `sha` instead. 
*Note: SHA comes in many "strengths". SHA1 or SHA256 are the most common. The examples below use SHA256*

### MD5
Liux command: `md5sum /path/to/file`
Example:
```bash
$ md5sum file.txt
1f9a6da4ca03c78e39bb83ff776c7ff5  file.txt
```

macOS command: `md5 /path/to/file`
Example:
```bash
$ md5 file.txt
MD5 (file.txt) = 12339025273e11fd70504ab836cf4e57
```


### SHA
Basic command: `shasum -a 256 /path/to/file`
Example:
```bash
$ shasum -a 256 file.txt
1f913b56912a373ea3bca5bf9fa5917af6f5fd11898774863fb4c48f024750bf  file.txt
```

Check command: `echo "hash File.name" |sha -a 256 /path/to/file`
Example:
```bash
$ echo "1f913b56912a373ea3bca5bf9fa5917af6f5fd11898774863fb4c48f024750bf  file.txt" |shasum -a 256 --check
file.txt: OK
```