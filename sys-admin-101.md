---
title: Sys Admin 101
description: A course like series of articles about how to be a Linux Systems Administrator
published: true
date: 2021-06-02T16:32:59.842Z
tags: base-page, sa-101
editor: markdown
dateCreated: 2021-06-02T15:54:13.303Z
---

The goal of this site is to help "everyone" to run a server in their home. Just showing how to run a webserver or file sync server on a little Raspberry Pi plugged into a router would be a disservice to those who'd like to set one up. By standing up your own server at home you are now, in effect, a systems administrator (sys admin) but without the training.

This series of articles and videos should help teach you to think like a sys admin and give the base skills to learn and practice on being your own SysAdmin @ Home!

## Lessons
* Coming Soon