---
title: Authenticate to Active Directory on Ubuntu 20.04
description: How to configure Ubuntu 20.04 or later to auth against AD
published: false
date: 2021-06-04T18:27:16.740Z
tags: auth, linux, active-directory, ad, single-sign-on, enterprise, article
editor: markdown
dateCreated: 2021-06-04T18:24:30.499Z
---

To simplify the authentication system, all Linux systems should be connected to our Active Directory (AD) domain. This article outlines how to connect a Red Hat based systems, ie. Oracle Linux, and Ubuntu systems to AD. This will allow domain admins--users with .adm at the end of their username--to SSH into a Linux server.

The following article assumes that you have the Linux host with a local admin user already set up and just need to bind it to the domain. All commands below need to be ran with sudo/root permissions.

Active Directory has a limit on the length of a hostname. Make sure before joining the realm that the hostname of the box is 15 or less characters prior to joining the realm.

Instructions 
The following instructions will install the tools needed to bind to an Active Directory server and configure the local system to allow domain users to SSH into the system. Before editing config files make a backup so it’s easier to roll back if there is a problem.

Install the following dependencies.

Oracle Linux

yum install sssd realmd oddjob oddjob-mkhomedir adcli samba-common samba-common-tools krb5-workstation openldap-clients policycoreutils-python-utils

Ubuntu

apt install realmd sssd sssd-tools adcli samba-common-bin krb5-user oddjob oddjob-mkhomedir packagekit

When prompted for the ‘Default Kerberos version 5 realm’ enter: CORP.VERISYS.COM

Update the /etc/krb5.conf file.

Oracle: includedir /etc/krb5.conf.d/

Ubuntu:  includedir /var/lib/sss/pubconf/krb5.include.d/


includedir <look at the lines above for what goes here>

[logging]
 default = FILE:/var/log/krb5libs.log
 kdc = FILE:/var/log/krb5kdc.log
 admin_server = FILE:/var/log/kadmind.log

[libdefaults]
 default_realm = CORP.VERISYS.COM
 dns_lookup_realm = true
 ticket_lifetime = 24h
 renew_lifetime = 7d
 forwardable = true
 rdns = true
 pkinit_anchors = FILE:/etc/pki/tls/certs/ca-bundle.crt
 default_ccache_name = KEYRING:persistent:%{uid}


[realms]
 CORP.VERISYS.COM = {
 kdc = corp.verisys.com:88
 }


[domain_realm]
 corp.verisys.com = CORP.VERISYS.COM
 .corp.verisys.com = CORP.VERISYS.COM
Bind to the Active Directory server.

realm join -v --user=<your.admin.domain.username> corp.verisys.com

You  can verify the connection by running the command realm list

Locate the file /etc/sssd/sssd.conf and make the following changes.

Oracle Linux & Ubuntu

Change use_fully_qualified_names = True to use_fully_qualified_names = False

Change fallback_homedir = /home/%u@%d to fallback_homedir = /home/%u

Ubuntu

Add the following lines under the ‘[domain/corp.verisys.com]’ section

ad_domain = corp.verisys.com

ad_gpo_access_control = permissive

Remove the line services = nss, pam from the file

Ubuntu only: Run the following command to enable automatic creation of home directory when logging in for the first time.

sudo pam-auth-update --enable mkhomedir

Ubuntu only: Disable the AppArmor profile for sssd.

ln -s /etc/apparmor.d/usr.sbin.sssd /etc/apparmor.d/disable/

apparmor_parser -R /etc/apparmor.d/usr.sbin.sssd

Create a file in the sudoers.d folder to give AD domain admin users sudo access.

touch /etc/sudoers.d/domain_admins

Add the following two lines to the domains_admins file you just created.


%domain\ admins ALL=(ALL)       ALL
%domain\ admins@corp.verisys.com ALL=(ALL)       ALL
Verify in the /etc/ssh/sshd_config file has the correct authentication settings. Locate the following lines and make sure they match what is listed below.

PasswordAuthentication yes

PermitRootLogin no

GSSAPIAuthentication yes

GSSAPICleanupCredentials no

Restart all of the systems needed to enable AD authentication.

systemctl restart sshd

systemctl restart sssd

systemctl daemon-reload

Verify everything is working.

Check that you’re connected to the directory with: realm list

Check that the Linux system can look up user data with: id fisrt.last.adm # Use your admin AD username here

In a new terminal window (don’t close your existing one) SSH into the box using your admin AD username, ssh first.last.adm@ip.of.host

This step is for overall AD management, login to an AD server, like VD01, and then move the new hosts into the correct path.

Troubleshooting
Though everything above should work, there are times something weird occurs and below are a few tips that may help.

If there is an issue with the AD bind, like an AD group clobbers a local group, you can always disconnect from the directory with the following command: realm leave -v

If you try to configure the sssd.conf or krb5.conf files and they are missing, verify that you’ve actually joined the directory first, as those files are created after joining the directory.

If you find that some users can authenticate via AD but not others, it could be that the hostname for the box is too long (AD only allows up to 15 characters ). If this is the case, rename the host to be 15 or less characters, leave the realm (make sure you do this as the root user), then join again.

Verify Kerberos can authenticate against AD: kinit -V user.name.adm