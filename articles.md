---
title: Articles
description: More long form/multi-step documents noted while setting up servers
published: true
date: 2021-06-04T18:26:15.830Z
tags: base-page, article
editor: markdown
dateCreated: 2021-05-14T05:29:17.226Z
---

This page is the base page for the Articles section and display all of the existing child pages in alphabetical order.

## SysAdmin @ Home Articles
- Authenticate to AD on Ubuntu 20.04 -- Coming Soon

## Digital Ocean Articles
- [How To Sync Calendars and Contacts with Baïkal on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-sync-calendars-and-contacts-using-carddav-and-caldav-standards-with-baikal-on-ubuntu-14-04)
- [How To Set Up a Masterless Puppet Environment on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-masterless-puppet-environment-on-ubuntu-14-04)
- [How To Install the Lita Chat Bot for IRC on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-install-the-lita-chat-bot-for-irc-on-ubuntu-14-04)
- [How To Set Up Multi-Factor Authentication for SSH on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-multi-factor-authentication-for-ssh-on-ubuntu-14-04)
- [How To Host a File Sharing Server with Pydio on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-host-a-file-sharing-server-with-pydio-on-ubuntu-14-04)
- [How To Set Up Multi-Factor Authentication for SSH on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-multi-factor-authentication-for-ssh-on-ubuntu-16-04)
- [How To Set Up Multi-Factor Authentication for SSH on CentOS 7](https://www.digitalocean.com/community/tutorials/how-to-set-up-multi-factor-authentication-for-ssh-on-centos-7)
- [How To Install Your Own Webmail Client with Roundcube on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-your-own-webmail-client-with-roundcube-on-ubuntu-16-04)
- [How To Secure Roundcube on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-secure-roundcube-on-ubuntu-16-04)
- [How to Verify Downloaded Files](https://www.digitalocean.com/community/tutorials/how-to-verify-downloaded-files)
- [How To Route Web Traffic Securely Without a VPN Using a SOCKS Tunnel](https://www.digitalocean.com/community/tutorials/how-to-route-web-traffic-securely-without-a-vpn-using-a-socks-tunnel)
- [How To Set Up Multi-Factor Authentication for SSH on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-multi-factor-authentication-for-ssh-on-ubuntu-18-04)
- [How To Set Up Multi-Factor Authentication for SSH on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-multi-factor-authentication-for-ssh-on-ubuntu-20-04)
- [How To Set Up Multi-Factor Authentication for SSH on CentOS 8](https://www.digitalocean.com/community/tutorials/how-to-set-up-multi-factor-authentication-for-ssh-on-centos-8)
