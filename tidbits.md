---
title: Tidbits
description: One-off commands useful for refreshing you memory on how to do something
published: true
date: 2021-06-08T02:44:59.903Z
tags: base-page, tidbit
editor: markdown
dateCreated: 2021-05-14T05:26:35.829Z
---

This page is the base page for the Tidbits section and display all of the existing child pages in alphabetical order.

- [Checksum Verification](/tidbits/checksum-verification)
- [Clearing RAM Cache & Swap](/tidbits/clearing-ram-cache-and-swap)
- [Create Swap File](/tidbits/create-swap-file)
- [Generate Public Cert ](/tidbits/generate-pub-cert)
- [Generate Public SSH Key](/tidbits/generate-pub-ssh-key)
- [Generate SSH Key Fingerprint](/tidbits/generate-ssh-fingerprint)
- [How to Flush DNS Cache](/tidbits/flush-dns-cache)
- [Parse JSON in BASH with jq](/tidbits/parse-json-in-bash)